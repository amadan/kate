# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2010, 2011, 2012, 2024 Freek de Kruijf <freekdekruijf@kde.nl>
# Freek de Kruijf <freekdekruijf@kde.nl>, 2011, 2013, 2014, 2018, 2020, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-26 00:41+0000\n"
"PO-Revision-Date: 2024-09-26 15:58+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 24.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "GDB-commando"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Zoekpaden van bronbestanden"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Lokale toepassing"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "Remote TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Seriële poort op afstand"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Host"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Poort"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Eigen initialisatiecommando's"

#: backend.cpp:27 backend.cpp:53 dapbackend.cpp:162
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"Een debug-sessie is gaande. Voer de huidige sessie opnieuw uit of stop deze."

#: configview.cpp:95
#, kde-format
msgid "Add new target"
msgstr "Nieuw doel toevoegen"

#: configview.cpp:99
#, kde-format
msgid "Copy target"
msgstr "Doel kopiëren"

#: configview.cpp:103
#, kde-format
msgid "Delete target"
msgstr "Doel verwijderen"

#: configview.cpp:108
#, kde-format
msgid "Executable:"
msgstr "Uitvoerbaar bestand:"

#: configview.cpp:128
#, kde-format
msgid "Working Directory:"
msgstr "Werkmap:"

#: configview.cpp:136
#, kde-format
msgid "Process Id:"
msgstr "Proces-ID:"

#: configview.cpp:141
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argumenten:"

#: configview.cpp:144
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Focus behouden"

#: configview.cpp:145
#, kde-format
msgid "Keep the focus on the command line"
msgstr "De focus op de commandoregel houden"

#: configview.cpp:147
#, kde-format
msgid "Redirect IO"
msgstr "IO omleiden"

#: configview.cpp:148
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr ""
"De IO van het programma dat wordt gedebugd omleiden naar een apart tabblad"

#: configview.cpp:150
#, kde-format
msgid "Advanced Settings"
msgstr "Geavanceerde instellingen"

#: configview.cpp:234
#, kde-format
msgid "Targets"
msgstr "Doelen"

#: configview.cpp:527 configview.cpp:540
#, kde-format
msgid "Target %1"
msgstr "Doel %1"

#: dapbackend.cpp:176
#, kde-format
msgid "DAP backend failed"
msgstr "DAP-backend is mislukt"

#: dapbackend.cpp:218
#, kde-format
msgid "program terminated"
msgstr "programma is beëindigd"

#: dapbackend.cpp:230
#, kde-format
msgid "requesting disconnection"
msgstr "verbinding verbreken is gevraagd"

#: dapbackend.cpp:244
#, kde-format
msgid "requesting shutdown"
msgstr "afsluiten is gevraagd"

#: dapbackend.cpp:268
#, kde-format
msgid "DAP backend: %1"
msgstr "DAP-backend: %1"

#: dapbackend.cpp:277 gdbbackend.cpp:654
#, kde-format
msgid "stopped (%1)."
msgstr "gestopt (%1)."

#: dapbackend.cpp:285 gdbbackend.cpp:658
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Actieve thread: %1 (alle threads gestopt)."

#: dapbackend.cpp:287 gdbbackend.cpp:660
#, kde-format
msgid "Active thread: %1."
msgstr "Actieve thread: %1"

#: dapbackend.cpp:292
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Breekpunt(en) bereikt:"

#: dapbackend.cpp:314
#, kde-format
msgid "(continued) thread %1"
msgstr "(doorgegaan) thread %1"

#: dapbackend.cpp:316
#, kde-format
msgid "all threads continued"
msgstr "alle threads zijn doorgegaan"

#: dapbackend.cpp:323
#, kde-format
msgid "(running)"
msgstr "(actief)"

#: dapbackend.cpp:409
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** verbinding met server gesloten ***"

#: dapbackend.cpp:416
#, kde-format
msgid "program exited with code %1"
msgstr "programma beëindigd met code %1"

#: dapbackend.cpp:434
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** wacht op acties van gebruiker ***"

#: dapbackend.cpp:439
#, kde-format
msgid "error on response: %1"
msgstr "fout in response: %1"

#: dapbackend.cpp:454
#, kde-format
msgid "important"
msgstr "belangrijk"

#: dapbackend.cpp:457
#, kde-format
msgid "telemetry"
msgstr "telemetrie"

#: dapbackend.cpp:476
#, kde-format
msgid "debugging process [%1] %2"
msgstr "process wordt gedebugd [%1] %2"

#: dapbackend.cpp:478
#, kde-format
msgid "debugging process %1"
msgstr "process %1 wordt gedebugd"

#: dapbackend.cpp:481
#, kde-format
msgid "Start method: %1"
msgstr "Startmethode: %1"

#: dapbackend.cpp:488
#, kde-format
msgid "thread %1"
msgstr "thread %1"

#: dapbackend.cpp:644
#, kde-format
msgid "breakpoint set"
msgstr "breekpunt ingesteld"

#: dapbackend.cpp:652
#, kde-format
msgid "breakpoint cleared"
msgstr "breekpunt gewist"

#: dapbackend.cpp:711
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) breekpunt"

#: dapbackend.cpp:728
#, kde-format
msgid "<not evaluated>"
msgstr "<niet geëvalueerd>"

#: dapbackend.cpp:750
#, kde-format
msgid "server capabilities"
msgstr "mogelijkheden van de server"

#: dapbackend.cpp:753
#, kde-format
msgid "supported"
msgstr "ondersteund"

#: dapbackend.cpp:753
#, kde-format
msgid "unsupported"
msgstr "niet-ondersteund"

#: dapbackend.cpp:756
#, kde-format
msgid "conditional breakpoints"
msgstr "breekpunten met voorwaarden"

#: dapbackend.cpp:757
#, kde-format
msgid "function breakpoints"
msgstr "functiebreekpunten"

#: dapbackend.cpp:758
#, kde-format
msgid "hit conditional breakpoints"
msgstr "breekpunten met voorwaarden verbergen"

#: dapbackend.cpp:759
#, kde-format
msgid "log points"
msgstr "punten loggen"

#: dapbackend.cpp:759
#, kde-format
msgid "modules request"
msgstr "verzoeken uit modules"

#: dapbackend.cpp:760
#, kde-format
msgid "goto targets request"
msgstr "ga naar verzoeken van doelen"

#: dapbackend.cpp:761
#, kde-format
msgid "terminate request"
msgstr "verzoek beëindigen"

#: dapbackend.cpp:762
#, kde-format
msgid "terminate debuggee"
msgstr "onderwerp van debug beëindigen"

#: dapbackend.cpp:980
#, kde-format
msgid "syntax error: expression not found"
msgstr "syntaxisfout: expressie niet gevonden"

#: dapbackend.cpp:998 dapbackend.cpp:1033 dapbackend.cpp:1071
#: dapbackend.cpp:1105 dapbackend.cpp:1141 dapbackend.cpp:1177
#: dapbackend.cpp:1213 dapbackend.cpp:1313 dapbackend.cpp:1375
#, kde-format
msgid "syntax error: %1"
msgstr "syntaxisfout: %1"

#: dapbackend.cpp:1006 dapbackend.cpp:1041 dapbackend.cpp:1320
#: dapbackend.cpp:1383
#, kde-format
msgid "invalid line: %1"
msgstr "ongeldige regel: %1"

#: dapbackend.cpp:1013 dapbackend.cpp:1018 dapbackend.cpp:1048
#: dapbackend.cpp:1053 dapbackend.cpp:1344 dapbackend.cpp:1349
#: dapbackend.cpp:1390 dapbackend.cpp:1395
#, kde-format
msgid "file not specified: %1"
msgstr "bestand niet gespecificeerd: %1"

#: dapbackend.cpp:1083 dapbackend.cpp:1117 dapbackend.cpp:1153
#: dapbackend.cpp:1189 dapbackend.cpp:1225
#, kde-format
msgid "invalid thread id: %1"
msgstr "ongeldig thread-id: %1"

#: dapbackend.cpp:1089 dapbackend.cpp:1123 dapbackend.cpp:1159
#: dapbackend.cpp:1195 dapbackend.cpp:1231
#, kde-format
msgid "thread id not specified: %1"
msgstr "thread-id niet gespecificeerd: %1"

#: dapbackend.cpp:1242
#, kde-format
msgid "Available commands:"
msgstr "Beschikbare commando's:"

#: dapbackend.cpp:1330
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "breekpunten met voorwaarden worden niet ondersteund door de server"

#: dapbackend.cpp:1338
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""
"breekpunten met voorwaarden verbergen wordt niet ondersteund door de server"

#: dapbackend.cpp:1358
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "regel %1 heeft al een breekpunt"

#: dapbackend.cpp:1403
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "breekpunt niet gevonden (%1:%2)"

#: dapbackend.cpp:1409
#, kde-format
msgid "Current thread: "
msgstr "Huidige thread: "

#: dapbackend.cpp:1414 dapbackend.cpp:1421 dapbackend.cpp:1445
#, kde-format
msgid "none"
msgstr "geen"

#: dapbackend.cpp:1417
#, kde-format
msgid "Current frame: "
msgstr "Huidig frame: "

#: dapbackend.cpp:1424
#, kde-format
msgid "Session state: "
msgstr "Status van sessie: "

#: dapbackend.cpp:1427
#, kde-format
msgid "initializing"
msgstr "initialiseren"

#: dapbackend.cpp:1430
#, kde-format
msgid "running"
msgstr "draait"

#: dapbackend.cpp:1433
#, kde-format
msgid "stopped"
msgstr "gestopt"

#: dapbackend.cpp:1436
#, kde-format
msgid "terminated"
msgstr "beëindigd"

#: dapbackend.cpp:1439
#, kde-format
msgid "disconnected"
msgstr "verbinding verbroken"

#: dapbackend.cpp:1442
#, kde-format
msgid "post mortem"
msgstr "post mortem"

#: dapbackend.cpp:1498
#, kde-format
msgid "command not found"
msgstr "commando niet gevonden"

#: dapbackend.cpp:1532
#, kde-format
msgid "missing thread id"
msgstr "ontbrekende thread-id"

#: dapbackend.cpp:1638
#, kde-format
msgid "killing backend"
msgstr "backend wordt afgebroken"

#: dapbackend.cpp:1696
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Huidig frame [%3]: %1:%2 (%4)"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr "Gebruikersinstellingen voor debugadapter"

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, kde-format
msgid "Settings File:"
msgstr "Instellingenbestand:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr "Standaard instellingen voor debugadapter"

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, kde-format
msgid "Debugger"
msgstr "Debugger"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr "Geen JSON-gegevens om te valideren."

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr "JSON-gegevens zijn geldig."

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "JSON-gegevens zijn ongeldig: geen JSON-object"

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "JSON-gegevens zijn ongeldig: %1"

#: gdbbackend.cpp:35
#, kde-format
msgid "Locals"
msgstr "Locals"

#: gdbbackend.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "CPU-registers"

#: gdbbackend.cpp:158
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""
"Zet het uitvoerbare programma in het tabblad 'Instellingen' in het paneel "
"'Debug'."

#: gdbbackend.cpp:167
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"Geen debugger geselecteerd. Selecteer er een in het tabblad 'Instellingen' "
"in het paneel 'Debug'."

#: gdbbackend.cpp:176
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"Debugger niet gevonden. Ga na dat het u het in uw systeem hebt "
"geïnstalleerd. De geselecteerde debugger is '%1'"

#: gdbbackend.cpp:383
#, kde-format
msgid "Could not start debugger process"
msgstr "Kon debuggerproces niet starten"

#: gdbbackend.cpp:441
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb is normaal beëindigd ***"

#: gdbbackend.cpp:647
#, kde-format
msgid "all threads running"
msgstr "alle threads zijn actief"

#: gdbbackend.cpp:649
#, kde-format
msgid "thread(s) running: %1"
msgstr "thread(s) actief: %1"

#: gdbbackend.cpp:679
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Huidig frame: %1:%2"

#: gdbbackend.cpp:706
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Host: %1. Doel %1"

#: gdbbackend.cpp:1376
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: kon laatste respons niet ontleden: %1. Te veel achtereenvolgende "
"fouten. Aan het afsluiten."

#: gdbbackend.cpp:1378
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: kon laatste respons niet ontleden: %1"

#: localsview.cpp:19
#, kde-format
msgid "Symbol"
msgstr "Symbool"

#: localsview.cpp:20
#, kde-format
msgid "Value"
msgstr "Waarde"

#: localsview.cpp:43
#, kde-format
msgid "type"
msgstr "type"

#: localsview.cpp:52
#, kde-format
msgid "indexed items"
msgstr "geïndexeerde items"

#: localsview.cpp:55
#, kde-format
msgid "named items"
msgstr "items met naam"

#: plugin_kategdb.cpp:104
#, kde-format
msgid "Kate Debug"
msgstr "Kate-debug"

#: plugin_kategdb.cpp:108
#, kde-format
msgid "Debug View"
msgstr "Debugweergave"

#: plugin_kategdb.cpp:108 plugin_kategdb.cpp:348
#, kde-format
msgid "Debug"
msgstr "Debuggen"

#: plugin_kategdb.cpp:111 plugin_kategdb.cpp:114
#, kde-format
msgid "Locals and Stack"
msgstr "Lokalen en stapel"

#: plugin_kategdb.cpp:166
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nr"

#: plugin_kategdb.cpp:166
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Frame"

#: plugin_kategdb.cpp:198
#, kde-format
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Debug-uitvoer"

#: plugin_kategdb.cpp:199
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Instellingen"

#: plugin_kategdb.cpp:241
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Kon bestand niet openen:</title><nl/>%1<br/>Probeer een zoekpad toe "
"te voegen aan Geavanceerde instellingen -> Zoekpaden van bronbestanden"

#: plugin_kategdb.cpp:266
#, kde-format
msgid "Start Debugging"
msgstr "Debugging starten"

#: plugin_kategdb.cpp:276
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Debugging afbreken / stoppen"

#: plugin_kategdb.cpp:283
#, kde-format
msgid "Continue"
msgstr "Doorgaan"

#: plugin_kategdb.cpp:289
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Breekpunt aan/uit"

#: plugin_kategdb.cpp:296
#, kde-format
msgid "Clear All Breakpoints"
msgstr "Alle breekpunten wissen"

#: plugin_kategdb.cpp:302
#, kde-format
msgid "Step In"
msgstr "Instappen"

#: plugin_kategdb.cpp:309
#, kde-format
msgid "Step Over"
msgstr "Overslaan"

#: plugin_kategdb.cpp:316
#, kde-format
msgid "Step Out"
msgstr "Uitstappen"

#: plugin_kategdb.cpp:323 plugin_kategdb.cpp:355
#, kde-format
msgid "Run To Cursor"
msgstr "Uitvoeren tot cursor"

#: plugin_kategdb.cpp:330
#, kde-format
msgid "Restart Debugging"
msgstr "Debugging opnieuw starten"

#: plugin_kategdb.cpp:338 plugin_kategdb.cpp:357
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "PC verplaatsen"

#: plugin_kategdb.cpp:343
#, kde-format
msgid "Print Value"
msgstr "Afdrukwaarde"

#: plugin_kategdb.cpp:442 plugin_kategdb.cpp:458
#, kde-format
msgid "Insert breakpoint"
msgstr "Breekpunt invoegen"

#: plugin_kategdb.cpp:456
#, kde-format
msgid "Remove breakpoint"
msgstr "Breekpunt verwijderen"

#: plugin_kategdb.cpp:606
#, kde-format
msgid "Execution point"
msgstr "Punt van uitvoeren"

#: plugin_kategdb.cpp:749
#, kde-format
msgid "Thread %1"
msgstr "Thread %1"

#: plugin_kategdb.cpp:849
#, kde-format
msgid "IO"
msgstr "IO"

#: plugin_kategdb.cpp:933
#, kde-format
msgid "Breakpoint"
msgstr "Breekpunt"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Debug"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:30
#, kde-format
msgid "Debug Plugin"
msgstr "Plugin voor debug"

#~ msgid "popup_breakpoint"
#~ msgstr "popup-_breakpoint"

#~ msgid "popup_run_to_cursor"
#~ msgstr "popup-_tot_cursor_uitvoeren"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Freek de Kruijf - t/m 2022"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "freekdekruijf@kde.nl"

#~ msgid "GDB Integration"
#~ msgstr "GDB-integratie"

#~ msgid "Kate GDB Integration"
#~ msgstr "Kate-GDB-integratie"

#~ msgid "/dev/ttyUSB0"
#~ msgstr "/dev/ttyUSB0"

#~ msgid "9600"
#~ msgstr "9600"

#~ msgid "14400"
#~ msgstr "14400"

#~ msgid "19200"
#~ msgstr "19200"

#~ msgid "38400"
#~ msgstr "38400"

#~ msgid "57600"
#~ msgstr "57600"

#~ msgid "115200"
#~ msgstr "115200"

#~ msgid "&Target:"
#~ msgstr "&Doel:"

#~ msgctxt "Program argument list"
#~ msgid "&Arg List:"
#~ msgstr "&Arg-lijst:"

#~ msgid "Remove Argument List"
#~ msgstr "Lijst met argumenten verwijderen"

#~ msgid "Arg Lists"
#~ msgstr "Arg-lijst"

#~ msgid "Add Working Directory"
#~ msgstr "Werkmap toevoegen"

#~ msgid "Remove Working Directory"
#~ msgstr "Werkmap verwijderen"
